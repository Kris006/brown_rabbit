import React, { Component } from "react";
import Articles from "./Articles/Articles";
import classes from "./Main.module.css";
import SideBar from "./SideBar/SideBar";

class Main extends React.Component {
  render() {
    return (
      <div className={classes.Main}>
        <Articles />
        <SideBar />
      </div>
    );
  }
}

export default Main;
