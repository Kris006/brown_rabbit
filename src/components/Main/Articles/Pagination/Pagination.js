import React from "react";
import classes from "./Pagination.module.css";

const Pagination = ({
  postsPerPage,
  totalArticles,
  paginate,
  text,
  paginateArrow,
}) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalArticles / postsPerPage); i++) {
    pageNumbers.push(i);
  }
  console.log(pageNumbers);
  return (
    <div className={classes.Pagination}>
      <span> Page {text} out of 5</span>
      {pageNumbers.map((numbers, id) => {
        return (
          <button
            className={classes.PaginationButton}
            key={id}
            onClick={(e) => paginate(numbers)}
          >
            {numbers}
          </button>
        );
      })}
      <button onClick={paginateArrow}>&gt;</button>
    </div>
  );
};

export default Pagination;
