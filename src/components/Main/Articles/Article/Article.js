import React, { useState } from "react";
import classes from "./Article.module.css";

const Article = (props) => {
  const {
    imageSource,
    heading,
    publishedDate,
    description,
    fullText,
    dots,
  } = props.article;

  const [isExpanded, toggleIsExpanded] = useState(false);

  return (
    <div className={classes.Article}>
      <img src={imageSource} alt="cofee picture" />

      <div className={classes.Content}>
        <h2>{heading}</h2>
        <time>{publishedDate}</time>
        <p>
          {description}

          {isExpanded ? <span>{fullText}</span> : <span>{dots}</span>}
        </p>
        <button onClick={() => toggleIsExpanded(!isExpanded)}>
          {isExpanded ? "Read Less" : "Read More"}
        </button>
        <hr />
      </div>
    </div>
  );
};

export default Article;

/* 
const {
  description,
  heading,
  publishedDate,
  imageSource,
  fullText,
} = props.article;

const [isExpanded, toggleIsExpanded] = useState(false);

return (
  <div className={classes.Article}>
    <h1>{props.index + 1}</h1>
    <p>
      {description}
      {isExpanded ? <span>{fullText}</span> : null}
    </p>
    <button
      onClick={() => {
        toggleIsExpanded(!isExpanded);
      }}
    >
      Click me
    </button>
  </div>
);
}; */
