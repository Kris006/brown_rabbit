import React, { useState } from "react";
import Article from "./Article/Article";
import ArticleData from "../../Data/MainData/MainData";
import Pagination from "./Pagination/Pagination";

const Articles = (props) => {
  const [currentArticles, setCurrentArticles] = useState(1);
  const [postsPerPage] = useState(3);
  const endIndex = currentArticles * postsPerPage;
  const firstIndex = endIndex - postsPerPage;
  const paginate = (pageNumber) => setCurrentArticles(pageNumber);
  const arrowPagination = () => {
    if (currentArticles <= 4) {
      setCurrentArticles(currentArticles + 1);
    }
  };
  let pageNumbers = currentArticles;

  return (
    <React.Fragment>
      {ArticleData.slice(firstIndex, endIndex).map((article, index) => {
        return <Article key={index} article={article} index={index} />;
      })}
      <Pagination
        totalArticles={ArticleData.length}
        postsPerPage={postsPerPage}
        paginate={paginate}
        text={pageNumbers}
        paginateArrow={arrowPagination}
      />
    </React.Fragment>
  );
};

export default Articles;

/* 
 const [startIndex, setStartIndex] = useState(0);

  let endIndex = startIndex + 2;

  const decramentPage = () => {
    if (startIndex >= 2) {
      setStartIndex(startIndex - 2);
    }
  };





<React.Fragment>
{articles.map((article, index) => {
  if (index >= startIndex && index <= endIndex) {
    return <Article key={index} index={index} article={article} />;
  }
})}
<button
  onClick={() => {
    if (startIndex <= articles.length - 2) {
      setStartIndex(startIndex + 2);
    }
  }}
>
  +
</button>
<button style={{ marginLeft: "4rem" }} onClick={decramentPage}></button>
</React.Fragment> */
