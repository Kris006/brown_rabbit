import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import classes from "./Modal.module.css";
import Button from "@material-ui/core/Button";
import DehazeIcon from "@material-ui/icons/Dehaze";

import { withStyles } from "@material-ui/core/styles";

const MenuList = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const StyledMenu = withStyles({
    root: {
      backgroundColor: "black",
      color: "white",
      fontSize: "2rem",
      fontFamily: "EB Garamond",
      "&:hover": {
        color: "black",
      },
    },
  })(MenuItem);

  const StyledButton = withStyles({
    root: {
      height: "4.2rem",
      width: "4.2rem",
      marginLeft: "2rem",
    },
  })(DehazeIcon);

  return (
    <div className={classes.Modal}>
      <Button onClick={handleClick}>
        <StyledButton>
          {" "}
          <DehazeIcon className={classes.MenuIcon} />
        </StyledButton>
      </Button>
      <Menu
        id="fade-menu"
        classes={{ root: classes.root }}
        className={classes.menu}
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <StyledMenu>ABOUT NBC</StyledMenu>
        <StyledMenu>2011 EVENT</StyledMenu>
        <StyledMenu>NORDIC ROASTER</StyledMenu>
        <StyledMenu>RESULT</StyledMenu>
        <StyledMenu>LINKS</StyledMenu>
        <StyledMenu>CONTACT</StyledMenu>
      </Menu>
    </div>
  );
};

export default MenuList;
