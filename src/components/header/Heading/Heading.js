import React from "react";
import classes from "./Heading.module.css";
import logo from "../../../assets/Images/HeaderImage/Logo.png";

const Heading = () => {
  return (
    <div className={classes.Heading}>
      <img src={logo} alt="logo" />
    </div>
  );
};

export default Heading;
