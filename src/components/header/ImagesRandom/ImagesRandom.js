import React from "react";
import classes from "./ImagesRandom.module.css";
import picture from "../../../assets/Images/HeaderImage/picture-1.jpg";

const Image = () => {
  const test = Math.ceil(Math.random() * 4);

  return (
    <div className={classes.Image}>
      <img src={picture} alt="coffee" />
    </div>
  );
};

export default Image;
