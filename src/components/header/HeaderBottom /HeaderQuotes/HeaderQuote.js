import React from "react";
import classes from "./HeaderQuote.module.css";

const HeaderQuote = () => {
  return (
    <div className={classes.HeaderQuote}>
      <blockquote>
        "To create an environment in which knowledge
        <br /> about coffe and its sphere can be obtained"
      </blockquote>
    </div>
  );
};

export default HeaderQuote;
