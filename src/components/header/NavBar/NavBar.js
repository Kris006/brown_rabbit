import React from "react";
import classes from "./NavBar.module.css";

const NavBar = (props) => {
  return (
    <div className={classes.NavBar}>
      <ul>
        {props.nav.map((nav, id) => {
          return (
            <li key={id}>
              <a href="/">{nav}</a>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default NavBar;
