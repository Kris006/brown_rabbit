import React from "react";
import classes from "./SearchBar.module.css";
import SearchIcon from "@material-ui/icons/Search";

const SearchBar = (props) => {
  return (
    <div className={classes.SearchBar}>
      <form>
        <input type="search" placeholder="Search..." />
        <button>
          <SearchIcon className={classes.SearchIcon} />
        </button>
      </form>
    </div>
  );
};

export default SearchBar;
