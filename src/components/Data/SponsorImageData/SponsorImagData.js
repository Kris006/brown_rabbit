import React from "react";

const SponsorImages = [
  { name: "bonaqa", img: "../assets/SponsorImages/bonaqa.png" },
  { name: "bunn", img: "../assets/SponsorImages/bunn.png" },
  { name: "cafeDeColumbia", img: "../assets/SponsorImages/cafeDeColumbia.png" },
  { name: "cafetto", img: "../assets/SponsorImages/cafetto.png" },
  { name: "cask", img: "../assets/SponsorImages/cask.png" },
  { name: "colmbiana", img: "../assets/SponsorImages/colmbiana.png" },
  { name: "dalla", img: "../assets/SponsorImages/dalla.png" },
  { name: "egge", img: "../assets/SponsorImages/egge.png" },
  { name: "esuplly", img: "../assets/SponsorImages/esupply.png" },
  { name: "figjo", img: "../assets/SponsorImages/figjo.png" },
  { name: "gourmet", img: "../assets/SponsorImages/gourmet.png" },
  { name: "lincoln", img: "../assets/SponsorImages/lincoln.png" },
  { name: "mahlkonig", img: "../assets/SponsorImages/mahlkonig.png" },
  { name: "mercanta", img: "../assets/SponsorImages/mercanta.png" },
  { name: "milk", img: "../assets/SponsorImages/milk.png" },
  { name: "moccamaster", img: "../assets/SponsorImages/moccamaster.png" },
  { name: "mojo", img: "../assets/SponsorImages/mojo.png" },
  { name: "nespresso", img: "../assets/SponsorImages/nespresso.png" },
  { name: "o", img: "../assets/SponsorImages/o.png" },
  { name: "olympia", img: "../assets/SponsorImages/olympia.png" },
  { name: "smartRoast", img: "../assets/SponsorImages/smartRoast.png" },
  { name: "trold", img: "../assets/SponsorImages/trold.png" },
  { name: "", img: "//:0" },
  { name: "", img: "//:0" },
];

export default SponsorImages;
