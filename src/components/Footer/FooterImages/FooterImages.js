import React from "react";
import classes from "./FooterImages.module.css";

const FooterImages = (props) => {
  const { name, img } = props.image;

  return (
    <React.Fragment>
      <img src={img} alt={name} className={classes.FooterImages} />
    </React.Fragment>
  );
};

export default FooterImages;
