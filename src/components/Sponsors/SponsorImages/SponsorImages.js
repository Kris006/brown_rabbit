import React from "react";
import classes from "./SponsorImages.module.css";

const SponsorImages = (props) => {
  const { name, img } = props.image;

  return (
    <div className={classes.ImageDiv}>
      <img className={classes.Image} src={img} alt={name} />
    </div>
  );
};

export default SponsorImages;
